import json
import os.path
import unittest
import uuid

from sklearn import datasets

from d3m_metadata import metadata as metadata_module
from d3m_metadata.container import dataset


def convert_metadata(metadata):
    return json.loads(json.dumps(metadata, cls=metadata_module.MetadataJsonEncoder))


class TestDataset(unittest.TestCase):
    def test_d3m(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'iris_dataset_1', 'datasetDoc.json'))

        ds = dataset.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        ds.metadata.check(ds)

        for row in ds['0']:
            for cell in row:
                # Nothing should be parsed from a string.
                self.assertIsInstance(cell, str)

        self.assertEqual(len(ds['0']), 150)
        self.assertEqual(len(ds['0'][0]), 6)

        self.assertEqual(convert_metadata(ds.metadata.query(())), {
            'schema': metadata_module.CONTAINER_SCHEMA_VERSION,
            'structural_type': 'd3m_metadata.container.dataset.Dataset',
            'id': 'iris_dataset_1',
            'version': '1.0',
            'name': 'Iris Dataset',
            'location_uris': [
                'file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path),
            ],
            'source': {
                'license': 'CC',
                'redacted': False,
                'human_subjects_research': False,
            },
            'dimension': {
                'length': 1,
                'name': 'resources',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/DatasetResource'],
            },
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('0',))), {
            'structural_type': 'numpy.ndarray',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/Table',
            ],
            'dimension': {
                'name': 'rows',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                'length': 150,
            }
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('0', metadata_module.ALL_ELEMENTS))), {
            'dimension': {
                'name': 'columns',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                'length': 6,
            }
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('0', metadata_module.ALL_ELEMENTS, 0))), {
            'name': 'd3mIndex',
            'structural_type': 'str',
            'semantic_types': [
                'http://schema.org/Integer',
                'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
            ],
        })

        for i in range(1, 5):
            self.assertEqual(convert_metadata(ds.metadata.query(('0', metadata_module.ALL_ELEMENTS, i))), {
                'name': ['sepalLength', 'sepalWidth', 'petalLength', 'petalWidth'][i - 1],
                'structural_type': 'str',
                'semantic_types': [
                    'http://schema.org/Float',
                    'https://metadata.datadrivendiscovery.org/types/Attribute',
                ],
            })

        self.assertEqual(convert_metadata(ds.metadata.query(('0', metadata_module.ALL_ELEMENTS, 5))), {
            'name': 'species',
            'structural_type': 'str',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/CategoricalData',
                'https://metadata.datadrivendiscovery.org/types/SuggestedTarget',
            ],
        })

    def test_csv(self):
        dataset_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'iris_dataset_1', 'tables', 'learningData.csv'))

        dataset_id = '219a5e7b-4499-4160-9b72-9cfa53c4924d'
        dataset_name = 'Iris Dataset'

        ds = dataset.Dataset.load('file://{dataset_path}'.format(dataset_path=dataset_path), dataset_id=dataset_id, dataset_name=dataset_name)

        ds.metadata.check(ds)

        for row in ds['0']:
            for cell in row:
                # Nothing should be parsed from a string.
                self.assertIsInstance(cell, str)

        self.assertEqual(len(ds['0']), 150)
        self.assertEqual(len(ds['0'][0]), 6)

        self.maxDiff = None

        self.assertEqual(convert_metadata(ds.metadata.query(())), {
            'schema': metadata_module.CONTAINER_SCHEMA_VERSION,
            'structural_type': 'd3m_metadata.container.dataset.Dataset',
            'id': dataset_id,
            'version': 'a5e827f2fb60639f1eb7b9bd3b849b0db9c308ba74d0479c20aaeaad77ccda48',
            'name': dataset_name,
            'stored_size': 4961,
            'location_uris': [
                'file://localhost{dataset_path}'.format(dataset_path=dataset_path),
            ],
            'dimension': {
                'length': 1,
                'name': 'resources',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/DatasetResource'],
            },
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('0',))), {
            'structural_type': 'numpy.ndarray',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/Table',
            ],
            'dimension': {
                'name': 'rows',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                'length': 150,
            }
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('0', metadata_module.ALL_ELEMENTS))), {
            'dimension': {
                'name': 'columns',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                'length': 6,
            }
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('0', metadata_module.ALL_ELEMENTS, 0))), {
            'name': 'd3mIndex',
            'structural_type': 'str',
        })

        for i in range(1, 5):
            self.assertEqual(convert_metadata(ds.metadata.query(('0', metadata_module.ALL_ELEMENTS, i))), {
                'name': ['sepalLength', 'sepalWidth', 'petalLength', 'petalWidth'][i - 1],
                'structural_type': 'str',
            })

        self.assertEqual(convert_metadata(ds.metadata.query(('0', metadata_module.ALL_ELEMENTS, 5))), {
            'name': 'species',
            'structural_type': 'str',
        })

    def test_sklearn(self):
        for dataset_path in ['boston', 'breast_cancer', 'diabetes', 'digits', 'iris', 'linnerud']:
            dataset.Dataset.load('sklearn://{dataset_path}'.format(dataset_path=dataset_path))

        dataset_uri = 'sklearn://iris'
        dataset_id = str(uuid.uuid3(uuid.NAMESPACE_URL, dataset_uri))
        dataset_name = 'Iris Dataset'

        ds = dataset.Dataset.load(dataset_uri, dataset_id=dataset_id, dataset_name=dataset_name)

        ds.metadata.check(ds)

        self.assertEqual(len(ds['0']), 150)
        self.assertEqual(len(ds['0'][0]), 5)

        self.assertEqual(convert_metadata(ds.metadata.query(())), {
            'schema': metadata_module.CONTAINER_SCHEMA_VERSION,
            'structural_type': 'd3m_metadata.container.dataset.Dataset',
            'id': '44f6efaa-72e7-383e-9369-64bd7168fb26',
            'version': '2c6c6d1f517e7da83457aa499bb5999585a25462dbd5c48d39d4140383ffa41e',
            'name': 'Iris Dataset',
            'location_uris': [
                dataset_uri,
            ],
            'description': datasets.load_iris()['DESCR'],
            'dimension': {
                'length': 1,
                'name': 'resources',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/DatasetResource'],
            },
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('0',))), {
            'structural_type': 'numpy.ndarray',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/Table',
            ],
            'dimension': {
                'name': 'rows',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                'length': 150,
            }
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('0', metadata_module.ALL_ELEMENTS))), {
            'dimension': {
                'name': 'columns',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                'length': 5,
            }
        })

        for i in range(0, 4):
            self.assertEqual(convert_metadata(ds.metadata.query(('0', metadata_module.ALL_ELEMENTS, i))), {
                'name': ['sepal length (cm)', 'sepal width (cm)', 'petal length (cm)', 'petal width (cm)'][i],
                'structural_type': 'numpy.float64',
                'semantic_types': [
                    'https://metadata.datadrivendiscovery.org/types/Attribute',
                ],
            })

        self.assertEqual(convert_metadata(ds.metadata.query(('0', metadata_module.ALL_ELEMENTS, 4))), {
            'structural_type': 'str',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/SuggestedTarget',
                'https://metadata.datadrivendiscovery.org/types/CategoricalData',
            ],
        })


if __name__ == '__main__':
    unittest.main()
