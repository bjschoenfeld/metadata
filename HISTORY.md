## vNEXT

## v2017.12.27

* Added `Params` class.
* Removed `Graph` class in favor of NetworkX `Graph` class.
* Added `Metadata` class with subclasses and documented the use of selectors.
* Added `Hyperparams` class.
* Added `Dataset` class.
* "Sequences" have generally been renamed to "containers". Related code is also now under
  `d3m_metadata.container` and not under `d3m_metadata.sequence` anymore.
* Package now requires Python 3.6.
* `__metadata__` attribute was renamed to `metadata`.
* Package renamed from `d3m_types` to `d3m_metadata`.
* Repository migrated to gitlab.com and made public.
* Added schemas for metadata contexts.
* A problem schema parsing and Python enumerations added in
  `d3m_metadata.problem` module.
* A standard set of container and base types have been defined.
